const allNewsBtn = document.getElementById("allNews");
const kyrgyzstanNewsBtn = document.getElementById("kyrgyzstanNews");
const russiaNewsBtn = document.getElementById("russiaNews");
const sportNewsBtn = document.getElementById("sportNews");
const techNewsBtn = document.getElementById("techNews");
const healthNewsBtn = document.getElementById("healthNews");

const news = [
  {
    id: 1,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Hi-Tech",
    time: "12 ч. назад",
    image: "http://localhost:8080/a668802335758f6afff5.png",
  },
  {
    id: 2,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Кыргызстан",
    time: "24.05.23",
    image: "http://localhost:8080/a0d54becd65a61f4c137.png",
  },
  {
    id: 3,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Кыргызстан",
    time: "24.05.23",
    image: "http://localhost:8080/ab3f5a94f5ebd2749953.png",
  },
  {
    id: 4,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Россия",
    time: "24.05.23",
    image: "http://localhost:8080/31b1207b0efa8a80f005.png",
  },
  {
    id: 5,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Здоровье",
    time: "24.05.23",
    image: "http://localhost:8080/8ae7c37baa9fd93e2f81.png",
  },
  {
    id: 6,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Кыргызстан",
    time: "24.05.23",
    image: "http://localhost:8080/aa5df2e14b6c25e4c3bb.png",
  },
  {
    id: 7,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Здоровье",
    time: "24.05.23",
    image: "http://localhost:8080/d77595fe76ae1a914bb2.png",
  },
  {
    id: 8,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Кыргызстан",
    time: "24.05.23",
    image: "http://localhost:8080/a668802335758f6afff5.png",
  },
  {
    id: 9,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Кыргызстан",
    time: "24.05.23",
    image: "http://localhost:8080/c95780301bb97f89b705.png",
  },
  {
    id: 10,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Hi-Tech",
    time: "12 ч. назад",
    image: "http://localhost:8080/a0d54becd65a61f4c137.png",
  },
  {
    id: 11,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Кыргызстан",
    time: "24.05.23",
    image: "http://localhost:8080/ab3f5a94f5ebd2749953.png",
  },
  {
    id: 12,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Кыргызстан",
    time: "24.05.23",
    image: "http://localhost:8080/31b1207b0efa8a80f005.png",
  },
  {
    id: 13,
    title: "Какое-то событие",
    description: "Классический текст Lorem Ipsum",
    type: "Россия",
    time: "24.05.23",
    image: "http://localhost:8080/8ae7c37baa9fd93e2f81.png",
  },
];

function displayNews(newsItems) {
  mainBlock.innerHTML = "";
  if (newsItems == []) {
    console.log("fsadf");
    mainBlock.innerHTML += `<h1>Нечего не найдено</h1>`;
  } else {
    newsItems.map((item) => {
      mainBlock.innerHTML += ` 
    <a class="col-md-3" href="/News-more.html">
    <div class="card mb-4 box-shadow border-0">
      <img src=${item.image} />
      <div class="py-3 w-100">
        <div class="newsTnC d-flex justify-content-between">
          <p>${item.time}</p>
          <p>${item.type}</p>
        </div>
        <h4 class="newsIvent">${item.title}</h4>
        <p class="card-text py-2">${item.description}</p>
      </div>
    </div>
  </a>`;
    });
  }
}
displayNews(news);

function sortNewsByType(selectedType) {
  const filteredNews = news.filter((item) => item.type === selectedType);
  displayNews(filteredNews);
}

allNewsBtn.addEventListener("click", () => displayNews(news));
kyrgyzstanNewsBtn.addEventListener("click", () => sortNewsByType("Кыргызстан"));
russiaNewsBtn.addEventListener("click", () => sortNewsByType("Россия"));
sportNewsBtn.addEventListener("click", () => sortNewsByType("Спорт"));
techNewsBtn.addEventListener("click", () => sortNewsByType("Hi-Tech"));
healthNewsBtn.addEventListener("click", () => sortNewsByType("Здоровье"));
