document.addEventListener('DOMContentLoaded', () => {
    const btns = document.querySelectorAll('.payments_card_btn');
    const instructionDesc = document.querySelector('.payments_instruction_desc');
    let activeBtnIndex = 0;

    function setActiveButton(index) {
        btns[activeBtnIndex].classList.remove('active');
        btns[index].classList.add('active');
        activeBtnIndex = index;
    }

    function updateInstructions(text) {
        instructionDesc.innerHTML = `<span>1. </span>${text}`;
    }

    const instructions = [
        'В любом терминале Дос-Кредобанк выбираете на экране "Финансовые услуги"',
        'В любом терминале Pay24 выбираете на экране "Финансовые услуги"',
        'В любом терминале QUICKPAY выбираете на экране "Финансовые услуги"',
        'В любом терминале UMAI выбираете на экране "Финансовые услуги"',
        'В любом терминале KICB выбираете на экране "Финансовые услуги"',
        'В любом терминале Банк Компаньон выбираете на экране "Финансовые услуги"',
        'В любом терминале Оной выбираете на экране "Финансовые услуги"',
        'В любом терминале Optima24 выбираете на экране "Финансовые услуги"',
        'В любом терминале Элкарт выбираете на экране "Финансовые услуги"',
        'В любом терминале РСК Банк выбираете на экране "Финансовые услуги"',
        'В любом терминале Finca Bank выбираете на экране "Финансовые услуги"',
        'В любом терминале 0! выбираете на экране "Финансовые услуги"',
        'В любом терминале Керемет Банк выбираете на экране "Финансовые услуги"',
    ];

    btns.forEach((btn, idx) => {
        btn.addEventListener('click', () => {
            updateInstructions(instructions[idx]);
            setActiveButton(idx);
        });
    });

    updateInstructions(instructions[0]);
    setActiveButton(0);
});
