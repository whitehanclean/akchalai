const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const devServer = (isDev) =>
  !isDev
    ? {}
    : {
        devServer: {
          open: true,
          hot: true,
          port: 8080,
        },
      };

module.exports = ({ develop }) => ({
  mode: develop ? "development" : "production",
  entry: {
    main: "./src/index.js",
    slider: "./src/pages/Main/slider.js",
    step: "./src/pages/Main/Step.js",
    reset: "./src/pages/ResetPassword/script.js",
    payments: "./src/pages/Payments/Payments.js",
    news: "./src/pages/News/index.js",
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "js/[name].[contenthash].js",
    clean: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      chunks: ["main"],
    }),

    new MiniCssExtractPlugin({
      filename: "./styles/main.css",
    }),
    // Pages
    new HtmlWebpackPlugin({
      template: "./src/pages/Main/Main.html",
      filename: "Main.html",
      chunks: ["slider", "main", "step"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/AboutUs/AboutUs.html",
      filename: "AboutUs.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/Contact-us/Contact-us.html",
      filename: "Contact-us.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/Contact-form/Contact-form.html",
      filename: "Contact-form.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/Documents/Documents.html",
      filename: "Documents.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/History/History.html",
      filename: "History.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/MyRepayments/MyRepayments.html",
      filename: "MyRepayments.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/News/News.html",
      filename: "News.html",
      chunks: ["news", "main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/News-more/News-more.html",
      filename: "News-more.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/Payments/Payments.html",
      filename: "Payments.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/PersonalData/PersonalData.html",
      filename: "PersonalData.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/PersonalDataMobile/PersonalDataMobile.html",
      filename: "PersonalDataMobile.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/Questions/Questions.html",
      filename: "Questions.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/Repayment/Repayment.html",
      filename: "Repayment.html",
      chunks: ["main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/ResetPassword/ResetPassword.html",
      filename: "ResetPassword.html",
      chunks: ["reset", "main"],
    }),
    new HtmlWebpackPlugin({
      template: "./src/pages/Operator/Operator.html",
      filename: "Operator.html",
      chunks: ["main"],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.(?:ico|png|jpg|jpeg|svg)$/i,
        type: "asset/inline",
      },
      {
        test: /\.html$/i,
        loader: "html-loader",
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.scss$/i,

        use: [
          MiniCssExtractPlugin.loader,
          "css-loader", // Преобразует CSS в модуль JavaScript
          "sass-loader", // Компилирует SCSS в CSS
        ],
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        include: path.resolve(
          __dirname,
          "./node_modules/bootstrap-icons/font/fonts"
        ),
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "webfonts",
            publicPath: "../webfonts",
          },
        },
      },
    ],
  },
  ...devServer(develop),
});
